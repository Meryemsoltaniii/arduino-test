int led=11;
void setup() {
  // put your setup code here, to run once:
  //definir led comme output
pinMode(led,OUTPUT);
//initialement led allumer
digitalWrite(led,LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
analogWrite(led,0); //eteindre led
delay(1000);
analogWrite(led,100); //allumer led a luminisite faible
delay(1000);
analogWrite(led,200);
delay(1000);
analogWrite(led,255);
delay(1000);
}
